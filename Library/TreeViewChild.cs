﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Dasic.Library
{
    internal class TreeViewChild : TreeViewItem
    {
        private readonly Label _strcoll = new Label();
        private readonly Label _strcomment = new Label();
        private readonly Label _strdefault = new Label();
        private readonly Label _strextra = new Label();
        private readonly Label _strfield = new Label();
        private readonly Label _strkey = new Label();
        private readonly Label _strnull = new Label();
        private readonly Label _strtype = new Label();

        public object StrField
        {
            get { return _strfield.Content; }
            set { _strfield.Content = value; }
        }

        public Brush StrFieldColor
        {
            get { return _strfield.Foreground; }
            set { _strfield.Foreground = value; }
        }

        public object StrType
        {
            get { return _strtype.Content; }
            set { _strtype.Content = value; }
        }

        public Brush StrTypeColor
        {
            get { return _strtype.Foreground; }
            set { _strtype.Foreground = value; }
        }


        public object StrColl
        {
            get { return _strcoll.Content; }
            set { _strcoll.Content = value; }
        }

        public Brush StrCollColor
        {
            get { return _strcoll.Foreground; }
            set { _strcoll.Foreground = value; }
        }

        public object StrNull
        {
            get { return _strnull.Content; }
            set { _strnull.Content = value; }
        }

        public Brush StrNullColor
        {
            get { return _strnull.Foreground; }
            set { _strnull.Foreground = value; }
        }

        public object StrKey
        {
            get { return _strkey.Content; }
            set { _strkey.Content = value; }
        }

        public Brush StrKeyColor
        {
            get { return _strkey.Foreground; }
            set { _strkey.Foreground = value; }
        }

        public object StrDefault
        {
            get { return _strdefault.Content; }
            set { _strdefault.Content = value; }
        }

        public Brush StrDefaultColor
        {
            get { return _strdefault.Foreground; }
            set { _strdefault.Foreground = value; }
        }

        public object StrExtra
        {
            get { return _strextra.Content; }
            set { _strextra.Content = value; }
        }

        public Brush StrExtraColor
        {
            get { return _strextra.Foreground; }
            set { _strextra.Foreground = value; }
        }

        public object StrComment
        {
            get { return _strcomment.Content; }
            set { _strcomment.Content = value; }
        }

        public Brush StrCommentColor
        {
            get { return _strcomment.Foreground; }
            set { _strcomment.Foreground = value; }
        }
    }
}